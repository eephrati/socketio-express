var express = require('express');
var router = express.Router();
var io = require('../config/socketio.js');

io.on('connection', function (socket) {
  socket.on('clientsent', function (data) {
    console.log(data);
    socket.emit('serversent', { hello: 'index'});
  });
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
